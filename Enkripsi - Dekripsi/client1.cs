﻿using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;

namespace TCP_Decompress
{
    class Program 
    {
        public static int port = 10000;

        private static string path = @"E:\Dev\C#\Jarkom\Enkripsi - Dekripsi\hasil";
        static void Main(string[] args)
        {
            Console.WriteLine("Press Enter to start receviing file");
            Console.ReadKey();
            ReceiveFiles();
            Console.ReadKey();
            DirectoryInfo directorySelect = new DirectoryInfo(path);
            foreach (FileInfo fileToDecompress in directorySelect.GetFiles("*.gz"))
            {
                Decompress(fileToDecompress);
            }
            Console.ReadKey();
        }

        public static void ReceiveFiles()
        {
            try
            {
                TcpClient tcpClnt = new TcpClient();
                Console.WriteLine("Connecting....");
                tcpClnt.Connect("127.0.0.1", port);
                Console.WriteLine("Connected");
                Stream stream = tcpClnt.GetStream();
                Console.WriteLine("Receiving File...");

                while (true)
                {
                    using (var output = File.Create("E:\\Dev\\C#\\Jarkom\\Enkripsi - Dekripsi\\hasil\\RecievedFile.gz"))
                    {
                        Console.WriteLine("Client connected. Starting to receive the file");

                        var buffer = new byte[1024];
                        int byteRead;
                        while ((byteRead = stream.Read(buffer,0,buffer.Length))>0)
                        {
                            output.Write(buffer,0,byteRead);
                        }
                        Console.WriteLine("File Received");
                        break;
                    }
                }
                tcpClnt.Close();
                Console.ReadKey();
            }
            catch (Exception e)
            {
                Console.WriteLine("Error..... " + e.StackTrace);
            }
        } 

        public static string MD5check(string filename)
        {
            var stream = File.OpenRead(filename);
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] retVal = md5.ComputeHash(stream);
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < retVal.Length; i++)
            {
                sb.Append(retVal[i].ToString("x2"));
            }
            stream.Close();
            return sb.ToString();
        }

        public static void Decompress(FileInfo fileToDecompress)
        {
            FileStream originalFileStream;
            FileStream decompressedFileStream;
            GZipStream decompressionStream;

            using (originalFileStream = fileToDecompress.OpenRead())
            {
                string currentFileName = fileToDecompress.FullName;
                string newFileName = currentFileName.Remove(currentFileName.Length  - fileToDecompress.Extension.Length);
                using (decompressedFileStream = File.Create(newFileName+ ".gz"))
                {
                    Console.WriteLine("Before decompressed = {0}", MD5check(originalFileStream.Name));
                    using (decompressionStream = new GZipStream(originalFileStream,CompressionMode.Decompress,false))
                    {
                        decompressionStream.CopyTo(decompressedFileStream);
                        Console.WriteLine("Decompressed {0}", fileToDecompress.Name);
                    }
                }
            }

            Console.WriteLine("After Decompressed = {0}", MD5check(decompressedFileStream.Name));
        }
    }
}