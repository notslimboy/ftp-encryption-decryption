using System;
using System.Text;
using System.Net;
using System.Net.Sockets;
using System.IO;
using System.IO.Compression;
using System.Security.Cryptography;

namespace TCP_Compression
{
    class Program
    {
        private static string path = @"E:\Dev\C#\Jarkom\Enkripsi - Dekripsi\file\";
        private static string filename = path + "LINE.apk";
        public static int port = 10000;
        private static FileInfo infoFile;
        private static FileStream compressedFileStream;
        private static FileStream originalFileStream;

        static void Main(string[] args)
        {
            DirectoryInfo directorySelect = new DirectoryInfo(path);
            Compress(directorySelect);
            Console.WriteLine("Press Enter to start sending file");
            Console.ReadKey();
            sendFiles();
        }

        public static void sendFiles()
        {
            try
            {
                IPAddress ipAdress = IPAddress.Parse("127.0.0.1");
                TcpListener listener = new TcpListener(ipAdress, port);
                listener.Start();
                Console.WriteLine("Waiting For Connection.....");
                Socket sock = listener.AcceptSocket();
                Console.WriteLine("Connection accept from = " + sock.RemoteEndPoint);

                sock.SendFile(filename);

                sock.Close();
                listener.Stop();

                Console.ReadKey();

            }
            catch (Exception e)
            {
                Console.WriteLine("Error..... " + e.StackTrace);
            }
        }

        public static void Compress(DirectoryInfo directorySelect)
        {
            foreach (FileInfo fileToCompress in directorySelect.GetFiles())
            {
                using (originalFileStream = fileToCompress.OpenRead())
                {
                    if ((File.GetAttributes(fileToCompress.FullName) & FileAttributes.Hidden) != FileAttributes.Hidden & fileToCompress.Extension != "gz")
                    {
                        Console.WriteLine("Before Compressed = {0}", MD5check(originalFileStream.Name));
                        using (compressedFileStream = File.Create(fileToCompress.FullName + ".gz"))
                        {
                            using (GZipStream compressStream = new GZipStream(compressedFileStream, CompressionMode.Compress, false))
                            {
                                originalFileStream.CopyTo(compressStream);
                            }
                            Console.WriteLine("After compressed = {0}", MD5check(compressedFileStream.Name));
                        }
                        infoFile = new FileInfo(path + "\\" + fileToCompress.Name + ".gz");
                        Console.WriteLine("Compressed {0} from {1} to {2} bytes ", fileToCompress.Name, fileToCompress.Length, infoFile.Length.ToString());
                    }
                }
            }
        }

        public static string MD5check(string fileName)
        {
            var stream = File.OpenRead(fileName);
            MD5 md5 = new MD5CryptoServiceProvider();
            byte[] retVal = md5.ComputeHash(stream);
            StringBuilder sb = new StringBuilder();

            for (int i = 0; i < retVal.Length; i++)
            {
                sb.Append(retVal[i].ToString("x2"));
            }
            stream.Close();
            return sb.ToString();
        }
    }
}
